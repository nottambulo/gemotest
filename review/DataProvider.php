<?php

//ToDo: namespace не соответствует стандартам PSR [http://svyatoslav.biz/misc/psr_translation/]
namespace src\Integration;

//ToDo: нет комментариев phpDoc
class DataProvider
{
    private $host;
    private $user;
    private $password;

    //ToDo: отсутствуют описания типов данных параметров, неполное заполнение phpDoc
    /**
     * @param $host
     * @param $user
     * @param $password
     */
    public function __construct($host, $user, $password)
    {
        $this->host = $host;
        $this->user = $user;
        $this->password = $password;
    }

    //ToDo: нет описания метода - неполное заполнение phpDoc
    //ToDo: имя метода не информативно, лучше getResponse
    //ToDo: если php >= 7.0, нужно добавить в метод тип возвращаемых данных
    /**
     * @param array $request
     *
     * @return array
     */
    public function get(array $request)
    {
        // returns a response from external service
    }
}
