<?php

//ToDo: namespace не соответствует стандартам PSR [http://svyatoslav.biz/misc/psr_translation/]
namespace src\Decorator;

use DateTime;
use Exception;
use Psr\Cache\CacheItemPoolInterface;
use Psr\Log\LoggerInterface;
use src\Integration\DataProvider;

//ToDo: нет комментариев phpDoc
class DecoratorManager extends DataProvider
{
    public $cache;
    public $logger;

    //ToDo: в конструкторе нужно объявить logger по умолчанию
    /**
     * @param string $host
     * @param string $user
     * @param string $password
     * @param CacheItemPoolInterface $cache
     */
    public function __construct($host, $user, $password, CacheItemPoolInterface $cache)
    {
        parent::__construct($host, $user, $password);
        $this->cache = $cache;
    }

    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * {@inheritdoc}
     */
    public function getResponse(array $input)
    {
        try {
            $cacheKey = $this->getCacheKey($input);
            $cacheItem = $this->cache->getItem($cacheKey);
            if ($cacheItem->isHit()) {
                return $cacheItem->get();
            }

            $result = parent::get($input);

            $cacheItem
                ->set($result)
                ->expiresAt(
                    (new DateTime())->modify('+1 day')
                );

            return $result;
        } catch (Exception $e) {
            //ToDo: добавить в конструктор
            $this->logger->critical('Error');
        }

        return [];
    }


    //ToDo: отсутствуют phpDoc
    //ToDo: если php >= 7.0, нужно добавить в метод тип возвращаемых данных
    public function getCacheKey(array $input)
    {
        return json_encode($input);
    }
}
