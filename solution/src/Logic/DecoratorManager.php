<?php

namespace Gemotest\Logic;

use DateTime;
use Exception;
use Monolog\Logger;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DecoratorManager
 *
 * @package GemotestSolution
 */
class DecoratorManager extends DataProvider
{
    public $cache;
    public $logger;


    /**
     * DecoratorManager constructor.
     *
     * @param array             $params -- Params [host, port, user]
     * @param FilesystemAdapter $cache  -- Cache Adapter
     * @param Logger            $logger -- Logger object
     */
    public function __construct(array $params, FilesystemAdapter $cache = null, Logger $logger = null)
    {
        if (!$this->checkParams($params)) {
            return false;
        }
        parent::__construct($params);

        $this->cache = $cache;
        if (null === $this->cache) {
            $this->cache = new FilesystemAdapter();
        }

        $this->logger = $logger;
        if (null === $this->logger) {
            $this->logger = new Logger('decorator_manager');
        }
    }

    /**
     * Set new Logger
     *
     * @param Logger $logger -- Logger
     *
     * @return bool
     */
    public function setLogger(Logger $logger): bool
    {
        $this->logger = $logger;
        return true;
    }

    /**
     * Get Response
     *
     * @param array $input -- Input params
     *
     * @return Response
     * @throws \Psr\Cache\InvalidArgumentException
     */
    public function getResponse(array $input): Response
    {
        try {
            $cacheKey = $this->getCacheKey($input);

            $cacheItem = $this->cache->getItem($cacheKey);

            if ($cacheItem->isHit()) {
                return $cacheItem->get();
            }
            $result = parent::getResponse($input);
            $cacheItem
                ->set($result)
                ->expiresAt(
                    (new DateTime())->modify('+1 day')
                );
            return new Response($result, 200);
        } catch (Exception $e) {
            $this->logger->critical('Error : ' . $e->getMessage());
        }

        return new Response('Something wrong', 500);
    }


    /**
     * Get cache key for array
     *
     * @param array $input -- input array
     *
     * @return string
     */
    public function getCacheKey(array $input): string
    {
        return json_encode($input);
    }


    /**
     * Check if construct params exists
     *
     * @param array $params -- params array
     *
     * @return bool
     */
    private function checkParams(array $params): bool
    {
        if (!array_key_exists('host', $params)
            || !array_key_exists('user', $params)
            || !array_key_exists('password', $params)
        ) {
            return false;
        }
        return true;
    }

}
