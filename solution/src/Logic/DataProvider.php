<?php

namespace Gemotest\Logic;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DataProvider
 *
 * @package GemotestSolution
 */
class DataProvider
{
    private $host;
    private $user;
    private $password;

    /**
     * DataProvider constructor.
     *
     * @param array $params -- input params
     */
    public function __construct(array $params)
    {
        $this->host = $params['host'];
        $this->user = $params['user'];
        $this->password = $params['password'];
    }

    /**
     * Get Response from external service
     *
     * @param array $params -- request params
     *
     * @return Response
     */
    public function getResponse(array $params): Response
    {
        return new Response(json_encode($params), 200);
    }
}
