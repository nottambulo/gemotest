<?php

namespace Gemotest;

use Symfony\Component\HttpFoundation\Response;
use Gemotest\Logic\DecoratorManager;

/**
 * Class Application
 *
 * @package Gemotest
 */
class Application
{

    public function run()
    {
        $requestParams = ['host' => 'https://www.gemotest.ru/test/', 'user' => 'test', 'password' => 'test'];
        $decoratorManager = new DecoratorManager($requestParams);

        try {
            $decoratorManager->getResponse($requestParams);
        } catch (\Exception $e) {
            return new Response($e->getMessage(), 500);
        }
    }

}
